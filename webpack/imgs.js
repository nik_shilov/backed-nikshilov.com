module.exports = function () {
  return {
    module: {
      rules: [
        {
          test: /.*\.(gif|png|jpe?g|svg)$/i,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '/img/[name]_[hash:7].[ext]',
              }
            },
          ]
        },
      ],
    },
  };
};